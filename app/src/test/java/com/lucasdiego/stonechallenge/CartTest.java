package com.lucasdiego.stonechallenge;

import com.lucasdiego.stonechallenge.data.models.Cart;
import com.lucasdiego.stonechallenge.data.models.CartItem;
import com.lucasdiego.stonechallenge.data.models.Product;
import com.lucasdiego.stonechallenge.view.utils.FormatHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Date;
import java.util.Locale;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by lucasdiego on 22/10/17.
 */

@RunWith(JUnit4.class)
public class CartTest {

    private Cart cart;

    private Product product1;
    private Product product2;

    @Before
    public void setup(){

        cart = new Cart();

        product1 = new Product("Title Product 2", new Double(1020), "59-988190", "Lucas Diego", "", new Date());
        product2 = new Product("Title Product 2", new Double(3830), "59-988190", "Lucas Diego", "", new Date());

    }

    @Test
    public void canCreateEmptyShoppingCart(){
        assertEquals(0d, cart.getTotalCart().doubleValue());
    }

    @Test
    public void shouldbeFalseIfCartIsEmpty(){
        assertEquals(false, cart.isNotEmpty());
//        assertTrue(cart.isNotEmpty());
    }

    @Test
    public void canAddItensToCart(){

        cart.addItem(new CartItem(product1, 1, product1.getPrice()));
        cart.addItem(new CartItem(product2, 2, product2.getPrice()));

        assertEquals(2, cart.getItems().size());
    }

    @Test
    public void checkCartTotalValueAfterAddOrRemoveItem(){

        //add one item
        cart.addItem(new CartItem(product1, 1, product1.getPrice()));

        Double total = product1.getPrice()/100;

        //compare value
        assertEquals(total, cart.getTotalCart().doubleValue());

        //add another item
        cart.addItem(new CartItem(product2, 2, product2.getPrice()));

        //calc total value and compare it again
        total += 2 * (product2.getPrice()/100);
        assertEquals(total, cart.getTotalCart().doubleValue() );

        //remove one unit from amount of item's card
        cart.addOrRemove(0, -1);

        total -= product1.getPrice()/100;
        assertEquals(total, cart.getTotalCart().doubleValue() );

        //add one unit in cart's position 1
        cart.addOrRemove(1, 1);
        total += product2.getPrice()/100;
        assertEquals(total, cart.getTotalCart().doubleValue() );
    }

    @Test
    public void checkIfTotalCartIsProperlyFormatted(){
        cart.addItem(new CartItem(product1, 1, product1.getPrice()));
        assertEquals("R$ 10,20", FormatHelper.formatCurrency(cart.getTotalCart(), new Locale("pt", "BR")));
    }

    @Test
    public void compratingDoublePrimitiveValeuWithDoubleClassValue(){

        double d1 = 0;
        Double d2 = new Double(0);
        Double d3 = new Double(1);

        assertEquals(d1, d2);
        assertTrue(d3 > 0);

    }


}
