package com.lucasdiego.stonechallenge;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.MediumTest;
import android.support.test.runner.AndroidJUnit4;

import com.lucasdiego.stonechallenge.data.models.CreditCard;
import com.lucasdiego.stonechallenge.data.models.Payment;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmConfiguration;

import static junit.framework.Assert.assertTrue;

/**
 * Created by lucasdiego on 20/10/17.
 */

@MediumTest
@RunWith(AndroidJUnit4.class)
public class PaymentPerformTest {

    private static Realm realm;
    private static Context context;

    @BeforeClass
    public static void setup(){
        RealmConfiguration testConfig =
                new RealmConfiguration.Builder().
                        inMemory().
                        name("test-realm").build();

        context = InstrumentationRegistry.getTargetContext().getApplicationContext();
        realm = Realm.getInstance(testConfig);
    }

    @Test
    public void shouldSaveAndRetrievePaymentData(){

        CreditCard creditCard = new CreditCard("1234567890098765", "234", "Fulano de Tal", "02/18");
        Payment payment = new Payment(creditCard, new Double(100), new Date());

        realm.executeTransaction(realm1 -> realm1.insertOrUpdate(payment));

        Payment retrievPayment = realm.where(Payment.class).equalTo("lastFourDigits", creditCard.getLastFourDigitsFromCardNumber()).findFirst();
        assertTrue(retrievPayment != null);
    }


}
