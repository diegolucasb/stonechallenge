package com.lucasdiego.stonechallenge;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.lucasdiego.stonechallenge.utils.RecyclerViewItemCountAssertion;
import com.lucasdiego.stonechallenge.view.activities.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertTrue;

/**
 * Created by lucasdiego on 24/10/17.
 */

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity>
            mActivityRule = new ActivityTestRule<>(MainActivity.class, false, true);


    @Test
    public void whenActivityIsLaunchedShouldDisplayProductListWithTenItems(){
        onView(withId(R.id.recyclerView)).check(new RecyclerViewItemCountAssertion(10));
    }

    @Test
    public void shouldAddItemOnCartWhenRecycleViewItemIsClicked(){
        onView(withId(R.id.recyclerView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(getRandomItem(), click()));

        onView(withId(R.id.fabCart)).perform(click());

        CustomApplication app = (CustomApplication) mActivityRule.getActivity().getApplication();

        assertTrue(app.getUserCart().getItems().size() > 0);
    }

    private int getRandomItem(){
        return new Random().nextInt(10);
    }


}
