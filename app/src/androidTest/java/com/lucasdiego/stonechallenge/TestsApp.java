package com.lucasdiego.stonechallenge;

import com.lucasdiego.stonechallenge.data.DataModule;
import com.lucasdiego.stonechallenge.view.activities.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by lucasdiego on 24/10/17.
 */

public class TestsApp extends CustomApplication {

    @Singleton
    @Component(modules = {AppModule.class, DataModule.class})
    public interface TestAppComponent extends AppComponent{
        void inject(CustomApplication customApplication);
    }

    private TestAppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
//        component = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }
}
