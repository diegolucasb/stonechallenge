package com.lucasdiego.stonechallenge;

import android.app.Application;
import android.content.Context;

import com.lucasdiego.stonechallenge.data.models.Cart;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by lucasdiego on 29/09/17.
 */

public class CustomApplication extends Application{

    private AppComponent appComponent;
    private Cart userCart;

    @Override
    public void onCreate() {
        super.onCreate();
        initComponents();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public Cart getUserCart() {
        return userCart;
    }

    private void initComponents() {
        userCart = new Cart();

        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        appComponent.inject(this);

        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("stonechallenge.realm")
                .deleteRealmIfMigrationNeeded()
                .schemaVersion(BuildConfig.REALM_SCHEMA_VERSION)
                .build();
    }

    public static CustomApplication get(Context context) {
        return (CustomApplication) context.getApplicationContext();
    }


}
