package com.lucasdiego.stonechallenge.view.activities.cart;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.lucasdiego.stonechallenge.data.models.Cart;
import com.lucasdiego.stonechallenge.data.models.Product;
import com.lucasdiego.stonechallenge.features.cart.models.CartItemPresentationModel;
import com.lucasdiego.stonechallenge.features.products.models.ProductPresentationModel;

import java.util.List;

/**
 * Created by lucasdiego on 01/10/17.
 */

public interface CartActivityContract {

    interface View{

        void showDataAdapter(List<CartItemPresentationModel> listProvided);

        void cartItemAddOrRemove(int position, int factor);

        void setTotalValue(String total);

        Context getContext();
    }

    interface Presenter{

        void loadData();

        void addOrRemoveItem(List<CartItemPresentationModel> listItems, int position, int factor);
    }


}
