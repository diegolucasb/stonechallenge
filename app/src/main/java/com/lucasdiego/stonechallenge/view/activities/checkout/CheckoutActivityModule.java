package com.lucasdiego.stonechallenge.view.activities.checkout;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lucasdiego on 30/09/17.
 */

@Module
public class CheckoutActivityModule {

    private final CheckoutActivityContract.View view;

    public CheckoutActivityModule(CheckoutActivityContract.View view) {
        this.view = view;
    }

    @Provides
    public CheckoutActivityContract.View provideView(){
        return this.view;
    }

    @Provides
    public CheckoutActivityContract.Presenter providePresenter(CheckoutActivityPresenter presenter){
        return presenter;
    }

}
