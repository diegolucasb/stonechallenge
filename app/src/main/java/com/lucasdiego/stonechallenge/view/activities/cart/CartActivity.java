package com.lucasdiego.stonechallenge.view.activities.cart;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lucasdiego.stonechallenge.CustomApplication;
import com.lucasdiego.stonechallenge.R;
import com.lucasdiego.stonechallenge.features.cart.models.CartItemPresentationModel;
import com.lucasdiego.stonechallenge.features.cart.presentation.CartItemListAdapter;
import com.lucasdiego.stonechallenge.view.activities.BaseActivity;
import com.lucasdiego.stonechallenge.view.activities.checkout.CheckoutActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by lucasdiego on 01/10/17.
 */

public class CartActivity extends BaseActivity implements CartActivityContract.View {

    public static final int REQUEST_CODE = 9999;
    @Inject
    CartActivityContract.Presenter presenter;

    @BindView(R.id.emptyView)
    View emptyView;

    @BindView(R.id.recyclerView)
    RecyclerView listItems;

    @BindView(R.id.textTotalCart)
    TextView textTotalCart;

    CartActivityComponent component;
    CartItemListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent();

        presenter.loadData();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_cart;
    }

    @Override
    public void intiViews() {
        setupToolbar(R.string.cart_title);
        textTotalCart.setText("");

        listItems.setLayoutManager(new LinearLayoutManager(this));
        listItems.setHasFixedSize(true);
    }

    @OnClick(R.id.buttonFinish)
    public void buttonFinishClick(Button button){
        if(CustomApplication.get(this).getUserCart().isNotEmpty()){

            Intent it = new Intent(this, CheckoutActivity.class);
            startActivityForResult(it, REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE && resultCode == RESULT_OK){
            //payment ok, clear cart and finish this activity
            CustomApplication.get(this).getUserCart().clearCart();
            finish();
        }
    }


    private void setupComponent() {
        component = DaggerCartActivityComponent.builder()
                .appComponent(CustomApplication.get(this).getAppComponent())
                .cartActivityModule(new CartActivityModule(this))
                .build();
        component.inject(this);
    }


    @Override
    public void showDataAdapter(List<CartItemPresentationModel> listProvided) {
        emptyView.setVisibility((listProvided == null || listProvided.size()==0)?View.VISIBLE:View.INVISIBLE);

        adapter = new CartItemListAdapter(listProvided, this);
        listItems.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void cartItemAddOrRemove(int position, int factor) {
        presenter.addOrRemoveItem(adapter.getList(), position, factor);
    }

    @Override
    public void setTotalValue(String total) {
        textTotalCart.setText(total);
    }

    @Override
    public Context getContext() {
        return this;
    }
}
