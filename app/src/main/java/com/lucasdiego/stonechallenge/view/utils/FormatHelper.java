package com.lucasdiego.stonechallenge.view.utils;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by lucasdiego on 22/10/17.
 */

public class FormatHelper {

    public static String formatCurrency(Double value){
        return formatCurrency(value, null);
    }

    public static String formatCurrency(Double value, Locale locale){
        NumberFormat nf;
        if(locale != null){
            nf = NumberFormat.getCurrencyInstance(locale);
        }else{
            nf = NumberFormat.getCurrencyInstance();
        }
        nf.setMaximumFractionDigits(2);

        return nf.format(value);
    }

}
