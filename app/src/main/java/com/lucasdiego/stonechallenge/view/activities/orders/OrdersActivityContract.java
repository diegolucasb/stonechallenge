package com.lucasdiego.stonechallenge.view.activities.orders;

import android.content.Context;

import com.lucasdiego.stonechallenge.data.models.Payment;
import com.lucasdiego.stonechallenge.features.payment.model.OrderItemPresentationModel;

import java.util.List;

/**
 * Created by lucasdiego on 01/10/17.
 */

public interface OrdersActivityContract {

    interface View{

        void updateAdapterList(List<OrderItemPresentationModel> list);

    }

    interface Presenter{

        void getOrderList();
    }


}
