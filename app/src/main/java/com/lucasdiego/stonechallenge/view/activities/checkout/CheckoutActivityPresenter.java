package com.lucasdiego.stonechallenge.view.activities.checkout;

import android.util.Log;

import com.lucasdiego.stonechallenge.data.models.CreditCard;
import com.lucasdiego.stonechallenge.data.models.Payment;
import com.lucasdiego.stonechallenge.features.payment.usercase.PaymentPerform;

import java.util.Date;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by lucasdiego on 30/09/17.
 */

public class CheckoutActivityPresenter implements CheckoutActivityContract.Presenter {

    private final CheckoutActivityContract.View view;
    private final PaymentPerform paymentPerform;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();


    @Inject
    public CheckoutActivityPresenter(CheckoutActivityContract.View view,
                                     PaymentPerform paymentPerform) {
        this.view = view;
        this.paymentPerform = paymentPerform;
    }

    @Override
    public void performPayment(final Payment payment) {

        compositeDisposable.add(
                paymentPerform.PerformPayment(payment)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
//                                paymentResult -> paymentSuccessful(paymentResult),
                                paymentResult -> paymentSuccessful(payment), //TODO Don't know how to make apiary return the same object that was created, that's why I'm saving that one that was sent
                                error -> Log.e("Payment error", error.getMessage())
                        )
        );

    }

    private void paymentSuccessful(Payment paymentResult) {
        CreditCard creditCard = new CreditCard(paymentResult.getCardNumber(), paymentResult.getCardHolderName());
        paymentPerform.savePaymentData(
                new Payment(
                        creditCard,
                        paymentResult.getValue(),
                        new Date()
                ));
        view.paymentSccessful(paymentResult);
    }

    @Override
    public void clearDisposable() {
        compositeDisposable.clear();
    }
}
