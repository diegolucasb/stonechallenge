package com.lucasdiego.stonechallenge.view.activities;

import com.lucasdiego.stonechallenge.AppComponent;
import com.lucasdiego.stonechallenge.di.scope.ActivityScope;
import com.lucasdiego.stonechallenge.features.products.usercases.ProductsListUserCaseModule;

import dagger.Component;

/**
 * Created by lucasdiego on 30/09/17.
 */

@ActivityScope
@Component(dependencies = AppComponent.class,
        modules = {
                MainActivityModule.class,
                ProductsListUserCaseModule.class
        })
public interface MainActivityComponent {

    void inject(MainActivity target);

}
