package com.lucasdiego.stonechallenge.view.activities.cart;

import com.lucasdiego.stonechallenge.AppComponent;
import com.lucasdiego.stonechallenge.di.scope.ActivityScope;
import com.lucasdiego.stonechallenge.features.products.usercases.ProductsListUserCaseModule;
import com.lucasdiego.stonechallenge.view.activities.MainActivity;
import com.lucasdiego.stonechallenge.view.activities.MainActivityModule;

import dagger.Component;

/**
 * Created by lucasdiego on 30/09/17.
 */

@ActivityScope
@Component(dependencies = AppComponent.class,
        modules = {
                CartActivityModule.class
        })
public interface CartActivityComponent {

    void inject(CartActivity target);

}
