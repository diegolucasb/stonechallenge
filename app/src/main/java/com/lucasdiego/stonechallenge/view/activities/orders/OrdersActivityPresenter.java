package com.lucasdiego.stonechallenge.view.activities.orders;

import android.util.Log;

import com.lucasdiego.stonechallenge.data.models.Payment;
import com.lucasdiego.stonechallenge.features.payment.model.OrderItemPresentationModel;
import com.lucasdiego.stonechallenge.features.payment.usercase.PaymentPerform;
import com.lucasdiego.stonechallenge.view.activities.checkout.CheckoutActivityContract;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by lucasdiego on 30/09/17.
 */

public class OrdersActivityPresenter implements OrdersActivityContract.Presenter {

    private final OrdersActivityContract.View view;
    private final PaymentPerform paymentPerform;

    @Inject
    public OrdersActivityPresenter(OrdersActivityContract.View view,
                                   PaymentPerform paymentPerform) {
        this.view = view;
        this.paymentPerform = paymentPerform;
    }

    @Override
    public void getOrderList() {
        List<OrderItemPresentationModel> result = new ArrayList<>();
        for(Payment p : paymentPerform.listAllPayments()){
            result.add(new OrderItemPresentationModel(p.getLastFourDigits(), p.getValue(), p.getDate(), p.getCardHolderName()));
        }
        view.updateAdapterList(result);
    }
}
