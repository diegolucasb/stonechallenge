package com.lucasdiego.stonechallenge.view.activities.checkout;

import com.lucasdiego.stonechallenge.AppComponent;
import com.lucasdiego.stonechallenge.di.scope.ActivityScope;
import com.lucasdiego.stonechallenge.features.payment.usercase.PaymentUserCaseModule;
import com.lucasdiego.stonechallenge.view.activities.cart.CartActivity;
import com.lucasdiego.stonechallenge.view.activities.cart.CartActivityModule;

import dagger.Component;

/**
 * Created by lucasdiego on 30/09/17.
 */

@ActivityScope
@Component(dependencies = AppComponent.class,
        modules = {
                CheckoutActivityModule.class,
                PaymentUserCaseModule.class
        })
public interface CheckoutActivityComponent {

    void inject(CheckoutActivity target);

}
