package com.lucasdiego.stonechallenge.view.activities.orders;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lucasdiego on 30/09/17.
 */

@Module
public class OrdersActivityModule {

    private final OrdersActivityContract.View view;

    public OrdersActivityModule(OrdersActivityContract.View view) {
        this.view = view;
    }

    @Provides
    public OrdersActivityContract.View provideView(){
        return this.view;
    }

    @Provides
    public OrdersActivityContract.Presenter providePresenter(OrdersActivityPresenter presenter){
        return presenter;
    }

}
