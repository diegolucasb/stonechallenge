package com.lucasdiego.stonechallenge.view.activities.cart;

import com.lucasdiego.stonechallenge.view.activities.MainActivityContract;
import com.lucasdiego.stonechallenge.view.activities.MainActivityPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lucasdiego on 30/09/17.
 */

@Module
public class CartActivityModule {

    private final CartActivityContract.View view;

    public CartActivityModule(CartActivityContract.View view) {
        this.view = view;
    }

    @Provides
    public CartActivityContract.View provideView(){
        return this.view;
    }

    @Provides
    public CartActivityContract.Presenter providePresenter(CartActivityPresenter presenter){
        return presenter;
    }

}
