package com.lucasdiego.stonechallenge.view.activities.orders;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.lucasdiego.stonechallenge.CustomApplication;
import com.lucasdiego.stonechallenge.R;
import com.lucasdiego.stonechallenge.features.payment.model.OrderItemPresentationModel;
import com.lucasdiego.stonechallenge.features.payment.presentation.OrderListAdapter;
import com.lucasdiego.stonechallenge.view.activities.BaseActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * Created by lucasdiego on 01/10/17.
 */

public class OrdersActivity extends BaseActivity implements OrdersActivityContract.View {

    @Inject
    OrdersActivityPresenter presenter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.emptyView)
    View emptyView;

    OrdersActivityComponent component;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupCompoent();
        presenter.getOrderList();
    }

    private void setupCompoent() {
        component = DaggerOrdersActivityComponent.builder()
                .appComponent(CustomApplication.get(this).getAppComponent())
                .ordersActivityModule(new OrdersActivityModule(this))
                .build();

        component.inject(this);
    }

    @Override
    public void intiViews() {
        setupToolbar(R.string.orders_title);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_orders;
    }

    @Override
    public void updateAdapterList(List<OrderItemPresentationModel> list) {
        emptyView.setVisibility((list==null || list.size()==0)?View.VISIBLE:View.INVISIBLE);

        OrderListAdapter adapter = new OrderListAdapter(list);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }
}
