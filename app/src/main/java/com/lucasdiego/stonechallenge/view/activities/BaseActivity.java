package com.lucasdiego.stonechallenge.view.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.lucasdiego.stonechallenge.R;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lucasdiego on 01/10/17.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    @Nullable
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        ButterKnife.bind(this);
        intiViews();
    }

    public abstract void intiViews();

    public abstract int getLayout();

    public void setupToolbar(){
        setupToolbar(null);
    }

    public void setupToolbar(String title, String subTitle, boolean setDisplayHomeAsUpEnabled){
        if(toolbar != null){
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(setDisplayHomeAsUpEnabled);
            if(title != null){
                getSupportActionBar().setTitle(title);
            }
            if(subTitle != null){
                getSupportActionBar().setSubtitle(subTitle);
            }
        }
    }

    public void setupToolbar(int titleResource, int subtitleResource, boolean setDisplayHomeAsUpEnabled){
        setupToolbar(getString(titleResource), getString(subtitleResource), setDisplayHomeAsUpEnabled);
    }

    public void setupToolbar(int titleResource, boolean setDisplayHomeAsUpEnabled){
        setupToolbar(getString(titleResource), null, setDisplayHomeAsUpEnabled);
    }

    public void setupToolbar(int titleResource){
        String title = getString(titleResource);
        setupToolbar(title, null, true);
    }

    public void setupToolbar(String title){
        setupToolbar(title, null, true);
    }

    public void formValidate(final Runnable runIfOK) {

        Validator validator = new Validator(this);
        validator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                runIfOK.run();
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                for (ValidationError error : errors) {
                    View view = error.getView();
                    String message = error.getCollatedErrorMessage(BaseActivity.this);

                    // Display error messages ;)
                    if (view instanceof EditText) {
                        ((EditText) view).setError(message);
//                    } else {
//                        showError(message);
                    }
                }
            }
        });
        validator.validate();
    }



}
