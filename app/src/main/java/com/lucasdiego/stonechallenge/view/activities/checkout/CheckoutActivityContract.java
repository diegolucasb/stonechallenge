package com.lucasdiego.stonechallenge.view.activities.checkout;

import android.content.Context;

import com.lucasdiego.stonechallenge.data.models.Payment;

/**
 * Created by lucasdiego on 01/10/17.
 */

public interface CheckoutActivityContract {

    interface View{

        Context getContext();

        void paymentSccessful(Payment paymentResult);
    }




    interface Presenter{

        void performPayment(Payment payment);

        void clearDisposable();
    }


}
