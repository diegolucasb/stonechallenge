package com.lucasdiego.stonechallenge.view.activities;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.lucasdiego.stonechallenge.CustomApplication;
import com.lucasdiego.stonechallenge.data.models.Cart;
import com.lucasdiego.stonechallenge.data.models.CartItem;
import com.lucasdiego.stonechallenge.data.models.Product;
import com.lucasdiego.stonechallenge.features.products.usercases.GetProductsList;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by lucasdiego on 30/09/17.
 */

public class MainActivityPresenter implements MainActivityContract.Presenter {

    private final MainActivityContract.View view;
    private final GetProductsList getProductsList;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public MainActivityPresenter(MainActivityContract.View view,
                                 GetProductsList getProductsList) {
        this.view = view;
        this.getProductsList = getProductsList;
    }

    @Override
    public void loadData() {

        compositeDisposable.add(
            getProductsList.getList()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(
                            products -> view.showDataAdapter(products),
                            error -> Log.e("Products get list", error.getMessage())
                    )
        );
    }

    @Override
    public void clearDisposable() {
        compositeDisposable.clear();
    }

    @Override
    public void addItemToCart(Product itemClicked) {
        if(itemClicked != null){

            CustomApplication.get(view.getContext())
                    .getUserCart()
                    .addItem(new CartItem(itemClicked, 1, itemClicked.getPrice()));

            view.shotToastMessage(itemClicked.getTitle() + " foi adicionado ao carrinho");

        }
    }



}
