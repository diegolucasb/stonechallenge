package com.lucasdiego.stonechallenge.view.activities;

import android.content.Context;

import com.lucasdiego.stonechallenge.data.models.Product;
import com.lucasdiego.stonechallenge.features.products.models.ProductPresentationModel;

import java.util.List;

/**
 * Created by lucasdiego on 30/09/17.
 */

public interface MainActivityContract  {

    interface View{

        void showDataAdapter(List<ProductPresentationModel> listProvided);

        void itemAdapterClick(Product itemClicked);

        void shotToastMessage(String message);

        Context getContext();
    }

    interface Presenter{

        void loadData();

        void clearDisposable();

        void addItemToCart(Product itemClicked);
    }


}
