package com.lucasdiego.stonechallenge.view.activities.checkout;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.lucasdiego.stonechallenge.CustomApplication;
import com.lucasdiego.stonechallenge.R;
import com.lucasdiego.stonechallenge.data.models.Payment;
import com.lucasdiego.stonechallenge.view.activities.BaseActivity;
import com.mobsandgeeks.saripaar.annotation.CreditCard;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by lucasdiego on 01/10/17.
 */

public class CheckoutActivity extends BaseActivity implements CheckoutActivityContract.View {

    @Inject
    CheckoutActivityContract.Presenter presenter;

    @BindView(R.id.editNumber)
    @CreditCard
    EditText editNumber;

    @BindView(R.id.editMonth)
    @NotEmpty
    EditText editMonth;

    @BindView(R.id.editYear)
    @NotEmpty
    EditText editYear;

    @BindView(R.id.editCvv)
    @NotEmpty
    EditText editCvv;

    @BindView(R.id.editName)
    @NotEmpty
    EditText editName;

    @BindView(R.id.buttonConfirm)
    Button buttonConfirm;

    CheckoutActivityComponent component;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setupComponent();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.clearDisposable();
    }

    private void setupComponent() {
        component = DaggerCheckoutActivityComponent.builder()
                .appComponent(CustomApplication.get(this).getAppComponent())
                .checkoutActivityModule(new CheckoutActivityModule(this))
                .build();
        component.inject(this);
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void paymentSccessful(Payment paymentResult) {
        Toast.makeText(this, "Obrigado! Seu pagamento foi efetuado com sucesso", Toast.LENGTH_SHORT).show();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void intiViews() {
        editNumber.requestFocus();
    }

    @Override
    public int getLayout() {
        return R.layout.activity_checkout;
    }

    @OnClick(R.id.buttonConfirm)
    public void buttonFinishClick(Button button){
        formValidate(() -> presenter.performPayment(new Payment(
                new com.lucasdiego.stonechallenge.data.models.CreditCard(
                                editNumber.getText().toString(),
                                editCvv.getText().toString(),
                                editName.getText().toString(),
                                editMonth.getText().toString() + "/" + editYear.getText().toString()),
                        CustomApplication.get(CheckoutActivity.this).getUserCart().getTotalCart()
                        )));
    }

}
