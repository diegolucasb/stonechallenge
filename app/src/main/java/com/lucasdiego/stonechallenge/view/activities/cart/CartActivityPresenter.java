package com.lucasdiego.stonechallenge.view.activities.cart;

import android.util.Log;

import com.lucasdiego.stonechallenge.CustomApplication;
import com.lucasdiego.stonechallenge.data.models.CartItem;
import com.lucasdiego.stonechallenge.data.models.Product;
import com.lucasdiego.stonechallenge.features.cart.models.CartItemPresentationModel;
import com.lucasdiego.stonechallenge.features.payment.usercase.PaymentPerform;
import com.lucasdiego.stonechallenge.features.products.usercases.GetProductsList;
import com.lucasdiego.stonechallenge.view.activities.MainActivityContract;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by lucasdiego on 30/09/17.
 */

public class CartActivityPresenter implements CartActivityContract.Presenter {

    private final CartActivityContract.View view;

    @Inject
    public CartActivityPresenter(CartActivityContract.View view) {
        this.view = view;
    }

    @Override
    public void loadData() {

        List<CartItemPresentationModel> list = new ArrayList<>();
        for(CartItem item : CustomApplication.get(view.getContext())
                .getUserCart().getItems()){
            list.add(new CartItemPresentationModel(item.getProduct(), item.getAmmount()));
        }

        view.showDataAdapter(list);
        calcTotalCart(list);
    }

    @Override
    public void addOrRemoveItem(List<CartItemPresentationModel> listItems, int position, int factor) {
        CartItemPresentationModel item = listItems.get(position);
        if(item.getAmmount() == 1 && factor == -1){

            listItems.remove(position);
            CustomApplication.get(view.getContext()).getUserCart().remove(position);

        }else{
            item.addOrRemoveItem(factor);
            CustomApplication.get(view.getContext()).getUserCart().addOrRemove(position, factor);
        }

        view.showDataAdapter(listItems);
        calcTotalCart(listItems);
    }

    private void calcTotalCart(List<CartItemPresentationModel> list){
        Double total = 0d;

        for(CartItemPresentationModel item : list){
            total += item.getTotal();
        }

        view.setTotalValue(NumberFormat.getCurrencyInstance().format(total));
    }


}
