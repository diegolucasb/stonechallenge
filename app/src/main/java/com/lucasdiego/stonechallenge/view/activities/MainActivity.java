package com.lucasdiego.stonechallenge.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.lucasdiego.stonechallenge.CustomApplication;
import com.lucasdiego.stonechallenge.R;
import com.lucasdiego.stonechallenge.data.models.Product;
import com.lucasdiego.stonechallenge.features.products.models.ProductPresentationModel;
import com.lucasdiego.stonechallenge.features.products.presentation.ProductListAdapter;
import com.lucasdiego.stonechallenge.view.activities.cart.CartActivity;
import com.lucasdiego.stonechallenge.view.activities.orders.OrdersActivity;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements MainActivityContract.View {

    @Inject
    MainActivityContract.Presenter presenter;

    @BindView(R.id.recyclerView)
    RecyclerView listProducts;

    MainActivityComponent component;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupComponent();

        presenter.loadData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_orders, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.action_orders:
                Intent it = new Intent(this, OrdersActivity.class);
                startActivity(it);
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void intiViews() {
        setupToolbar(R.string.app_name, false);
        listProducts.setLayoutManager(new LinearLayoutManager(this));
        listProducts.setHasFixedSize(true);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }


    @OnClick(R.id.fabCart)
    public void cartActivity(){
        Intent intent = new Intent(this, CartActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.clearDisposable();
    }

    private void setupComponent() {
        component = DaggerMainActivityComponent.builder()
                .appComponent(CustomApplication.get(this).getAppComponent())
                .mainActivityModule(new MainActivityModule(this))
                .build();
        component.inject(this);

    }

    @Override
    public void showDataAdapter(List<ProductPresentationModel> listProvided) {
        ProductListAdapter adapter = new ProductListAdapter(listProvided, this);
        listProducts.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void itemAdapterClick(Product itemClicked) {
        presenter.addItemToCart(itemClicked);
    }

    @Override
    public void shotToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Context getContext() {
        return this;
    }
}
