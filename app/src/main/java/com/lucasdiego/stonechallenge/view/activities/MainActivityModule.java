package com.lucasdiego.stonechallenge.view.activities;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lucasdiego on 30/09/17.
 */

@Module
public class MainActivityModule {

    private final MainActivityContract.View view;

    public MainActivityModule(MainActivityContract.View view) {
        this.view = view;
    }

    @Provides
    public MainActivityContract.View provideView(){
        return this.view;
    }

    @Provides
    public MainActivityContract.Presenter providePresenter(MainActivityPresenter presenter){
        return presenter;
    }

}
