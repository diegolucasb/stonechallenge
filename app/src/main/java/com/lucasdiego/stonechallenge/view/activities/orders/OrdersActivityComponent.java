package com.lucasdiego.stonechallenge.view.activities.orders;

import com.lucasdiego.stonechallenge.AppComponent;
import com.lucasdiego.stonechallenge.di.scope.ActivityScope;
import com.lucasdiego.stonechallenge.features.payment.usercase.PaymentUserCaseModule;
import com.lucasdiego.stonechallenge.view.activities.checkout.CheckoutActivity;
import com.lucasdiego.stonechallenge.view.activities.checkout.CheckoutActivityModule;

import dagger.Component;

/**
 * Created by lucasdiego on 30/09/17.
 */

@ActivityScope
@Component(dependencies = AppComponent.class,
        modules = {
                OrdersActivityModule.class,
                PaymentUserCaseModule.class
        })
public interface OrdersActivityComponent {

    void inject(OrdersActivity target);

}
