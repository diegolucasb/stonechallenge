package com.lucasdiego.stonechallenge.features.payment.presentation;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lucasdiego.stonechallenge.R;
import com.lucasdiego.stonechallenge.features.payment.model.OrderItemPresentationModel;
import com.lucasdiego.stonechallenge.view.activities.orders.OrdersActivityContract;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lucasdiego on 30/09/17.
 */

public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderViewHolder> {

    private final List<OrderItemPresentationModel> list;

    public OrderListAdapter(List<OrderItemPresentationModel> list) {
        this.list = list;
    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new OrderViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_item_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(OrderViewHolder holder, int position) {
        OrderItemPresentationModel item = list.get(position);
        holder.render(item);
    }

    @Override
    public int getItemCount() {
        return list==null?0:list.size();
    }

    class OrderViewHolder extends RecyclerView.ViewHolder {

        View view;

        @BindView(R.id.textTitle)
        TextView title;

        @BindView(R.id.textCard)
        TextView card;

        @BindView(R.id.textTotal)
        TextView total;

        Context context;

        public OrderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            this.view = view;
        }

        void render(OrderItemPresentationModel model){

            title.setText(model.getDateString());
            card.setText(model.getCardIdentifier());
            total.setText(model.getFormattedTotal());
        }

    }

}
