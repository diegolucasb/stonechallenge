package com.lucasdiego.stonechallenge.features.cart.usercase;

import com.lucasdiego.stonechallenge.data.models.Cart;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by lucasdiego on 01/10/17.
 */

public class GetCartImpl implements GetCart {

    private final Realm realmIntance;

    @Inject
    public GetCartImpl(Realm realmIntance) {
        this.realmIntance = realmIntance;
    }


    @Override
    public int getNextSequence() {
//        if(realmIntance.where(Cart.class).findAll() == null){
            return 1;
//        }
//        return realmIntance.where(Cart.class).findAll().last().getId() + 1;
    }

    @Override
    public void save(Cart cart) {
//        realmIntance.executeTransaction(realm -> realm.insertOrUpdate(cart));
    }
}
