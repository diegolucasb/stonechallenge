package com.lucasdiego.stonechallenge.features.cart.usercase;

import com.lucasdiego.stonechallenge.data.models.Cart;

/**
 * Created by lucasdiego on 01/10/17.
 */

public interface GetCart {

    int getNextSequence();

    void save(Cart cart);

}
