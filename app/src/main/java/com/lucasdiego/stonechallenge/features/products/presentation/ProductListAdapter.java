package com.lucasdiego.stonechallenge.features.products.presentation;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.lucasdiego.stonechallenge.R;
import com.lucasdiego.stonechallenge.features.products.models.ProductPresentationModel;
import com.lucasdiego.stonechallenge.view.activities.MainActivityContract;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lucasdiego on 30/09/17.
 */

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ProductsViewHolder> {

    private final List<ProductPresentationModel> list;
    private final MainActivityContract.View delegateView;

    public ProductListAdapter(List<ProductPresentationModel> list, MainActivityContract.View delegateView) {
        this.list = list;
        this.delegateView = delegateView;
    }

    @Override
    public ProductsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductsViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.product_item_adapter, parent, false), parent.getContext());
    }

    @Override
    public void onBindViewHolder(ProductsViewHolder holder, int position) {
        ProductPresentationModel item = list.get(position);
        holder.render(item);
        holder.clickEvent(item);
    }

    @Override
    public int getItemCount() {
        return list==null?0:list.size();
    }

    class ProductsViewHolder extends RecyclerView.ViewHolder {

        View view;

        @BindView(R.id.imageProduct)
        ImageView imageProduct;

        @BindView(R.id.textTitle)
        TextView title;

        @BindView(R.id.textSeller)
        TextView seller;

        @BindView(R.id.textDate)
        TextView date;

        @BindView(R.id.textPrice)
        TextView price;

        Context context;

        public ProductsViewHolder(View view, Context context) {
            super(view);
            ButterKnife.bind(this, view);
            this.view = view;
            this.context = context;
        }


        void render(ProductPresentationModel model){

            Glide.with(context)
                    .load(model.getThumbnailHd())
                    .centerCrop()
                    .into(imageProduct);

            title.setText(model.getTitle());
            seller.setText(context.getString(R.string.seller) + model.getSeller());
            date.setText(model.getPublishedDate());
            price.setText(model.getFormattedPrice());


        }

        public void clickEvent(final ProductPresentationModel item) {
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    delegateView.itemAdapterClick(item);
                }
            });
        }
    }

}
