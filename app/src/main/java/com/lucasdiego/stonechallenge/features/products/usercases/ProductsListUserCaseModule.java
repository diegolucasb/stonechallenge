package com.lucasdiego.stonechallenge.features.products.usercases;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lucasdiego on 30/09/17.
 */

@Module
public class ProductsListUserCaseModule {

    @Provides
    public GetProductsList provideGetProductList(GetProductsListImpl usercase){
        return usercase;
    }

}
