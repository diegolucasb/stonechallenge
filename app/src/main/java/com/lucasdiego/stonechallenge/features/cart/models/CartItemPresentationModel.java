package com.lucasdiego.stonechallenge.features.cart.models;

import com.lucasdiego.stonechallenge.data.models.CartItem;
import com.lucasdiego.stonechallenge.data.models.Product;
import com.lucasdiego.stonechallenge.view.utils.FormatHelper;

import org.joda.time.format.FormatUtils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

/**
 * Created by lucasdiego on 01/10/17.
 */

public class CartItemPresentationModel extends CartItem{

    private String title;
    private String seller;
    private int ammount;
    private Double total;

    public String getProductImage() {
        return getProduct().getThumbnailHd();
    }

    public CartItemPresentationModel(Product product, int ammount) {
        super(product, ammount, product.getPrice());
        this.title = product.getTitle();
        this.seller =product.getSeller();
        this.ammount = ammount;
    }

    public String getTitle() {
        return title;
    }

    public String getSeller() {
        return seller;
    }

    public String getFormattedTotal() {
        return FormatHelper.formatCurrency(getTotal());
    }

    @Override
    public int getAmmount() {
        return ammount;
    }

    public Double getTotal() {
        total = getAmmount()*(getUnitPrice()/100);
        return total;
    }

    public void addOrRemoveItem(int factor) {
        ammount += factor;
    }


}
