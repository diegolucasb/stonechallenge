package com.lucasdiego.stonechallenge.features.products.usercases;

import com.lucasdiego.stonechallenge.features.products.models.ProductPresentationModel;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by lucasdiego on 30/09/17.
 */

public interface GetProductsList {

    Single<List<ProductPresentationModel>> getList();

}
