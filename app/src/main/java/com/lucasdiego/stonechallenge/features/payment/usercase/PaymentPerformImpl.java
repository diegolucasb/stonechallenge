package com.lucasdiego.stonechallenge.features.payment.usercase;

import com.lucasdiego.stonechallenge.BuildConfig;
import com.lucasdiego.stonechallenge.data.api.PaymentService;
import com.lucasdiego.stonechallenge.data.models.Payment;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by lucasdiego on 02/10/17.
 */

public class PaymentPerformImpl implements PaymentPerform {

    private final PaymentService paymentService;
    private final Realm realm;

    @Inject
    public PaymentPerformImpl(PaymentService paymentService, Realm realm) {
        this.paymentService = paymentService;
        this.realm = realm;
    }

    @Override
    public Single<Payment> PerformPayment(Payment payment) {
        return paymentService.performPayment(BuildConfig.PAYMENT_SERVER_URL,
                payment).singleOrError();
    }

    @Override
    public void savePaymentData(Payment payment){
        realm.executeTransaction(realm1 -> realm1.insertOrUpdate(payment));
    }

    @Override
    public List<Payment> listAllPayments() {
        return realm.where(Payment.class).findAll();
    }

}
