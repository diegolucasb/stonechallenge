package com.lucasdiego.stonechallenge.features.payment.usercase;

import com.lucasdiego.stonechallenge.data.models.Payment;

import java.util.List;

import io.reactivex.Single;

/**
 * Created by lucasdiego on 02/10/17.
 */

public interface PaymentPerform {

    Single<Payment> PerformPayment(Payment payment);

    void savePaymentData(Payment payment);

    List<Payment> listAllPayments();

}
