package com.lucasdiego.stonechallenge.features.payment.usercase;

import com.lucasdiego.stonechallenge.features.products.usercases.GetProductsList;
import com.lucasdiego.stonechallenge.features.products.usercases.GetProductsListImpl;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lucasdiego on 30/09/17.
 */

@Module
public class PaymentUserCaseModule {

    @Provides
    public PaymentPerform providePaymentPerform(PaymentPerformImpl paymentPerform){
        return paymentPerform;
    }

}
