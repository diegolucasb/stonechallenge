package com.lucasdiego.stonechallenge.features.payment.model;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by lucasdiego on 01/10/17.
 */

public class OrderItemPresentationModel {

    private String lastFourDigits;
    private Double value;
    private Date date;
    private String cardHolderName;

    public OrderItemPresentationModel(String lastFourDigits, Double value, Date date, String cardHolderName) {
        this.lastFourDigits = lastFourDigits;
        this.value = value;
        this.date = date;
        this.cardHolderName = cardHolderName;
    }

    public String getLastFourDigits() {
        return lastFourDigits;
    }

    public void setLastFourDigits(String lastFourDigits) {
        this.lastFourDigits = lastFourDigits;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getFormattedTotal() {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        nf.setMaximumFractionDigits(2);

        return "Total da compra " + nf.format(getValue());
    }

    public String getDateString() {
        return "Compra realizada em " + new SimpleDateFormat("dd/MM/yyyy ' às ' HH:mm").format(getDate());
    }

    public String getCardIdentifier(){
        return "Cartão " + getLastFourDigits() + " - " + getCardHolderName();
    }



}
