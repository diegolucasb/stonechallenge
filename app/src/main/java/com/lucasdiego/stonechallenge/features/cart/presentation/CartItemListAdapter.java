package com.lucasdiego.stonechallenge.features.cart.presentation;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.lucasdiego.stonechallenge.R;
import com.lucasdiego.stonechallenge.features.cart.models.CartItemPresentationModel;
import com.lucasdiego.stonechallenge.view.activities.cart.CartActivityContract;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lucasdiego on 30/09/17.
 */

public class CartItemListAdapter extends RecyclerView.Adapter<CartItemListAdapter.CartItemViewHolder> {

    private final List<CartItemPresentationModel> list;
    private final CartActivityContract.View delegateView;

    public CartItemListAdapter(List<CartItemPresentationModel> list, CartActivityContract.View delegateView) {
        this.list = list;
        this.delegateView = delegateView;
    }

    @Override
    public CartItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CartItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cart_item_adapter, parent, false), parent.getContext());
    }

    @Override
    public void onBindViewHolder(CartItemViewHolder holder, int position) {
        CartItemPresentationModel item = getItemListPosition(position);
        holder.render(item);
        holder.clickEvents(position);
    }

    public CartItemPresentationModel getItemListPosition(int position){
        return list.get(position);
    }

    public List<CartItemPresentationModel> getList() {
        return list;
    }

    public void removeItem(int position){
        list.remove(position);
        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return list==null?0:list.size();
    }

    class CartItemViewHolder extends RecyclerView.ViewHolder {

        View view;

        @BindView(R.id.imageProduct)
        ImageView imageProduct;

        @BindView(R.id.textTitle)
        TextView title;

        @BindView(R.id.textSeller)
        TextView seller;

        @BindView(R.id.buttonRemove)
        ImageView buttonRemove;

        @BindView(R.id.textViewAmmount)
        TextView textViewAmmount;

        @BindView(R.id.buttonAdd)
        ImageView buttonAdd;

        @BindView(R.id.textPrice)
        TextView price;

        Context context;

        public CartItemViewHolder(View view, Context context) {
            super(view);
            ButterKnife.bind(this, view);
            this.view = view;
            this.context = context;
        }

        void render(CartItemPresentationModel model){

            Glide.with(context)
                    .load(model.getProductImage())
                    .centerCrop()
                    .into(imageProduct);

            title.setText(model.getTitle());
            seller.setText(context.getString(R.string.seller) + model.getSeller());
            price.setText(model.getFormattedTotal());
            textViewAmmount.setText(String.valueOf(model.getAmmount()));
        }

        public void clickEvents(final int position) {

            View.OnClickListener buttonClick = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int factor = (v.getId() == R.id.buttonRemove)?-1:1;
                    delegateView.cartItemAddOrRemove(position, factor);
                }
            };

            buttonAdd.setOnClickListener(buttonClick);
            buttonRemove.setOnClickListener(buttonClick);

        }
    }

}
