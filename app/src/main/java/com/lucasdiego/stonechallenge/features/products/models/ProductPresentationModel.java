package com.lucasdiego.stonechallenge.features.products.models;

import com.lucasdiego.stonechallenge.data.models.Product;
import com.lucasdiego.stonechallenge.view.utils.FormatHelper;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by lucasdiego on 30/09/17.
 */

public class ProductPresentationModel extends Product{

    public ProductPresentationModel(String title, Double price, String zipcode, String seller, String thumbnailHd, Date date) {
        super(title, price, zipcode, seller, thumbnailHd, date);
    }

    public String getFormattedPrice() {
        return FormatHelper.formatCurrency(getPrice()/100);
//        NumberFormat nf = NumberFormat.getCurrencyInstance();
//        nf.setMaximumFractionDigits(2);
//
//        return nf.format(getPrice()/100);
    }

    public String getPublishedDate() {
        return new SimpleDateFormat("dd/MM/yyyy").format(getDate());
    }
}
