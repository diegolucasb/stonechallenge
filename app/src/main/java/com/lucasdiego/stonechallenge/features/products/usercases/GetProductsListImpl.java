package com.lucasdiego.stonechallenge.features.products.usercases;

import com.lucasdiego.stonechallenge.data.api.ProductsService;
import com.lucasdiego.stonechallenge.data.models.Product;
import com.lucasdiego.stonechallenge.features.products.models.ProductPresentationModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.realm.Realm;

/**
 * Created by lucasdiego on 30/09/17.
 */

public class GetProductsListImpl implements GetProductsList{

    private final ProductsService apiService;

    @Inject
    public GetProductsListImpl(ProductsService apiService) {
        this.apiService = apiService;
    }

    @Override
    public Single<List<ProductPresentationModel>> getList() {

        return apiService.getProducts()
                .flatMapIterable(p -> p)
                .map(p -> new ProductPresentationModel(p.getTitle(), p.getPrice(), p.getZipcode(), p.getSeller(), p.getThumbnailHd(), p.getDate()))
                .toList();
    }

}
