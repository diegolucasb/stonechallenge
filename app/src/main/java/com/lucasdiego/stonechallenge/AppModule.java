package com.lucasdiego.stonechallenge;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by lucasdiego on 29/09/17.
 */

@Module
public class AppModule {

    Context appContext;

    public AppModule(Application application) {
        appContext = application.getBaseContext();
    }

    @Provides
    @Singleton
    Context provideApplication(){
        return appContext;
    }

    @Provides
    @Singleton
    Resources provideAppResources() {
        return appContext.getResources();
    }

    @Provides
    @Singleton
    EventBus provideEventBus() {
        return EventBus.getDefault();
    }

    @Provides
    @Singleton
    Glide provideGlide(Context context){
        return Glide.get(context);
    }


}
