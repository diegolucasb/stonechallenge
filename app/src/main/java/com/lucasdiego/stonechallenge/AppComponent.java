package com.lucasdiego.stonechallenge;

import com.lucasdiego.stonechallenge.data.DataComponent;
import com.lucasdiego.stonechallenge.data.DataModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by lucasdiego on 29/09/17.
 */

@Singleton
@Component(modules = {AppModule.class, DataModule.class})
public interface AppComponent extends DataComponent {

    void inject(CustomApplication app);

}
