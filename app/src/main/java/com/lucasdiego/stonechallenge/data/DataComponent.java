package com.lucasdiego.stonechallenge.data;

import com.lucasdiego.stonechallenge.data.api.PaymentService;
import com.lucasdiego.stonechallenge.data.api.ProductsService;
import com.lucasdiego.stonechallenge.data.models.Payment;

import io.realm.Realm;

/**
 * Created by lucasdiego on 30/09/17.
 */

public interface DataComponent {

    ProductsService exposeProductApiService();

    PaymentService exposePaymentApiService();

    Realm exposeRealm();

}
