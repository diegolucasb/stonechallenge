package com.lucasdiego.stonechallenge.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.lucasdiego.stonechallenge.BuildConfig;
import com.lucasdiego.stonechallenge.data.api.PaymentService;
import com.lucasdiego.stonechallenge.data.api.ProductsService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.Realm;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by lucasdiego on 30/09/17.
 */

@Module
public class DataModule {

    @Provides
    @Singleton
    public ProductsService provideProductApi(Retrofit retrofit){
        return retrofit.create(ProductsService.class);
    }

    @Provides
    @Singleton
    public PaymentService providePaymentService(Retrofit retrofit){
        return retrofit.create(PaymentService.class);
    }

    @Provides
    @Singleton
    public Realm provideRealm(){
        return Realm.getDefaultInstance();
    }

    @Provides
    @Singleton
    public Converter.Factory provideGsonConverter(){
        Gson gson = new GsonBuilder()
                .setDateFormat("dd/MM/yyyy")
                .create();
        return GsonConverterFactory
                .create(gson);
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        httpClient.addInterceptor(logging);

        logging.setLevel(BuildConfig.LOGLEVEL);
        return httpClient.build();
    }

    @Provides
    @Singleton
    public Retrofit provideRetrofit(Converter.Factory converter, OkHttpClient client){

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        httpClient.addInterceptor(logging);

        logging.setLevel(BuildConfig.LOGLEVEL);

        return new Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_BASE_URL)
                .client(client)
                .addConverterFactory(converter)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }





}
