package com.lucasdiego.stonechallenge.data.api;

import com.lucasdiego.stonechallenge.data.models.Payment;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by lucasdiego on 01/10/17.
 */

public interface PaymentService {

    @POST
    Observable<Payment> performPayment(@Url String url, @Body Payment payment);


}
