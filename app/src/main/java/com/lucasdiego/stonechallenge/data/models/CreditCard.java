package com.lucasdiego.stonechallenge.data.models;

/**
 * Created by lucasdiego on 23/10/17.
 */

public class CreditCard {

    private String cardNumber;

    private String cvv;

    private String cardHolderName;

    private String expDate;

    public CreditCard() {
    }

    public CreditCard(String cardNumber, String cvv, String cardHolderName, String expDate) {
        this.cardNumber = cardNumber;
        this.cvv = cvv;
        this.cardHolderName = cardHolderName;
        this.expDate = expDate;
    }

    public CreditCard(String cardNumber, String cardHolderName) {
        this.cardNumber = cardNumber;
        this.cardHolderName = cardHolderName;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getLastFourDigitsFromCardNumber(){
        return getCardNumber()==null?"":getCardNumber().substring(getCardNumber().length()-4);
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }
}
