package com.lucasdiego.stonechallenge.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;

/**
 * Created by lucasdiego on 01/10/17.
 */

public class Payment extends RealmObject{

    @SerializedName("card_number")
    @Ignore
    private String cardNumber;

    private String lastFourDigits;

    private Double value;

    @Ignore
    private String cvv;

    private Date date;

    @SerializedName("card_holder_name")
    private String cardHolderName;

    @SerializedName("exp_date")
    @Ignore
    private String expDate;

    public Payment() {
    }

    public Payment(CreditCard creditCard, Double value, Date date) {
        this.lastFourDigits = creditCard.getLastFourDigitsFromCardNumber();
        this.value = value;
        this.date = date;
        this.cardHolderName = creditCard.getCardHolderName();
    }

    public Payment(CreditCard creditCard, Double value) {
        this.cardNumber = creditCard.getCardNumber();
        this.lastFourDigits = creditCard.getLastFourDigitsFromCardNumber();
        this.value = value;
        this.cvv = creditCard.getCvv();
        this.cardHolderName = creditCard.getCardHolderName();
        this.expDate = creditCard.getExpDate();
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setCardHolderName(String cardHolderName) {
        this.cardHolderName = cardHolderName;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getLastFourDigits() {
        return lastFourDigits;
    }

    public void setLastFourDigits(String lastFourDigits) {
        this.lastFourDigits = lastFourDigits;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }



}
