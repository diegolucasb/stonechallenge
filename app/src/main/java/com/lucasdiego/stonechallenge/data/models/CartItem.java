package com.lucasdiego.stonechallenge.data.models;

/**
 * Created by lucasdiego on 01/10/17.
 */

public class CartItem {

    private Product product;
    private int ammount;
    private Double unitPrice;
    private String observations;

    public CartItem() {
    }

    public CartItem(Product product, int ammount, Double unitPrice) {
        this.product = product;
        this.ammount = ammount;
        this.unitPrice = unitPrice;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getAmmount() {
        return ammount;
    }

    public void setAmmount(int ammount) {
        this.ammount = ammount;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CartItem cartItem = (CartItem) o;

        return product != null ? product.equals(cartItem.product) : cartItem.product == null;

    }

    @Override
    public int hashCode() {
        return product != null ? product.hashCode() : 0;
    }
}
