package com.lucasdiego.stonechallenge.data.api;

import com.lucasdiego.stonechallenge.data.models.Product;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * Created by lucasdiego on 30/09/17.
 */

public interface ProductsService {

    @GET("products.json")
    Observable<List<Product>> getProducts();

}
