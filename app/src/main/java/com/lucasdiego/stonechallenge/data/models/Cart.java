package com.lucasdiego.stonechallenge.data.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lucasdiego on 01/10/17.
 */

public class Cart {

    private List<CartItem> items;

    public Cart() {
        this.items = new ArrayList<>();
    }

    public List<CartItem> getItems() {
        return items;
    }

    public void addItem(CartItem item){
        this.items.add(item);
    }

    public void clearCart(){
        items.clear();
    }

    public Double getTotalCart(){
        Double total = 0d;
        for(CartItem item : getItems()){
            total += item.getAmmount() * (item.getUnitPrice()/100);
        }

        return total;
    }

    public void addOrRemove(int itemPosition, int factor){
        CartItem item = getItems().get(itemPosition);
        item.setAmmount(item.getAmmount() + factor);
    }

    public void remove(int itemPosition){
        getItems().remove(itemPosition);
    }

    public boolean isNotEmpty(){
        return !items.isEmpty();
    }

}
