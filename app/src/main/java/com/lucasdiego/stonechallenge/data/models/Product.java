package com.lucasdiego.stonechallenge.data.models;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by lucasdiego on 30/09/17.
 */

public class Product{

    private String title;
    private Double price;
    private String zipcode;
    private String seller;
    private String thumbnailHd;
    private Date date;

    public Product() {
    }

    public Product(String title, Double price, String zipcode, String seller, String thumbnailHd, Date date) {
        this.title = title;
        this.price = price;
        this.zipcode = zipcode;
        this.seller = seller;
        this.thumbnailHd = thumbnailHd;
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public String getThumbnailHd() {
        return thumbnailHd;
    }

    public void setThumbnailHd(String thumbnailHd) {
        this.thumbnailHd = thumbnailHd;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (!title.equals(product.title)) return false;
        return seller.equals(product.seller);

    }

    @Override
    public int hashCode() {
        int result = title.hashCode();
        result = 31 * result + seller.hashCode();
        return result;
    }
}
